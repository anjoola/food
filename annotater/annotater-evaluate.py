import json, os, platform, random, signal, sys, textwrap, time
import HTMLParser

reload(sys)
sys.setdefaultencoding('utf8')

BATCH_SIZE = 100
MENTION_FILE = "mentions.txt"
RESTAURANT_FILE = "restaurants.json"
REVIEW_FILE = "reviews.json"

IS_WINDOWS = "Windows" in platform.system()

batch_start = 0
restaurants = {}
# <list of (restaurant id, review index, mention)>
all_mentions = []

# <(restaurant id, review index, mention)> -> <menu item index>
annotated = {}
# <list of mentions already processed>
processed_mentions = []

# Output file.
output = None

class Restaurant:
  id = ""
  name = ""
  # <list of reviews>
  reviews = []
  # <review index> -> <list of mentions>
  mentions = {}
  # <list of menu (item name, item description)>
  menu_items = []

  def __init__(self, id, name):
    self.id = id
    self.name = name

def fmt(string, indent=""):
  print HTMLParser.HTMLParser().unescape(textwrap.fill(string, 80, subsequent_indent=indent).encode('utf-8'))


def get_partial_matches(mention_, menu_items):
  matches = []
  for idx, item_ in enumerate(menu_items):
    item = (item_[0].lower() + ("" if len(item_[1].strip()) < 1 else "---" + item_[1].lower())).split()
    mention_split = mention_.lower().split()

    words_matched = 0
    for word in mention_split:
      if word in item:
        words_matched += 1
      else:
        break

    if words_matched <= len(mention_split) / 2: continue

    matches.append((idx, " ".join(item)))
  return matches


def match_prompt(mention, num_matches):
  fmt("If <<" + mention + ">> were part of a food item name, which of " + \
      "the menu items above does it most closely match to?")

  match_number = -1
  tried = False
  while match_number == -1:
    try:
      if not tried:
        tried = True
        match_number = int(raw_input("Enter the number: "))
      else:
        match_number = int(raw_input("Please enter a number between 0-%d: " % (num_matches+1)))
      if match_number < 0 or match_number > (num_matches+1):
        match_number = -1
        continue
    except ValueError:
      match_number = -1

  # match_number is -1 for not a food item, -2 for can't tell.
  if match_number > 0 and match_number <= num_matches:
    return match_number
  elif match_number == 0:
    return -1
  else:
    return -2


def sentiment_prompt(mention):
  print "\nIs the sentiment of <<" + mention + ">> in this review:"
  print "1: Positive"
  print "2: Neutral"
  print "3: Negative"

  sentiment = -1
  tried = False
  while sentiment < 1 or sentiment > 3:
    try:
      if not tried:
        tried = True
        sentiment = int(raw_input("Enter the number: "))
      else:
        sentiment = int(raw_input("Please enter a number between 1-3: "))
      if sentiment < 1 or sentiment > 3:
        sentiment = -1
        continue
    except ValueError:
      sentiment = -1

  sentiment = "pos" if sentiment == 1 else ("neg" if sentiment == 3 else "neu")
  return sentiment


def annotate():
  global output
  output = open('results-%d.txt' % batch_start, 'w')

  count = 0
  for (restaurant_id, review_idx, mention) in all_mentions[batch_start:]:
    if mention.lower() in processed_mentions: continue

    restaurant = restaurants[restaurant_id]

    # Get the review and menu.
    review = restaurant.reviews[review_idx]
    menu_items = restaurant.menu_items

    mention_check = mention.encode(sys.stdout.encoding, errors='replace')
    review_check = review.encode(sys.stdout.encoding, errors='replace')
    if mention_check not in review_check: continue
    mention_idx = review.index(mention)

    # See if the mention is actually the restaurant name. If so, skip it.
    if mention.lower() in restaurant.name.lower(): continue

    # Get mention context.
    start_idx = review[0:mention_idx].rfind(".")
    end_idx = review[mention_idx + len(mention):].find(".")
    start_idx = start_idx + 1 if start_idx != -1 else 0
    end_idx = end_idx if end_idx == -1 else end_idx + mention_idx + len(mention)

    # Get menu item options.
    matches = get_partial_matches(mention, menu_items)
    if len(matches) == 0: continue
    if len(matches) > 10: matches = matches[0:10]

    # Clear screen.
    if IS_WINDOWS: os.system("cls")
    else:          os.system("clear")

    print "\n***Press CTRL + C at any time to quit***"
    print "\n[%d of %d]\n" % (count + 1, BATCH_SIZE)

    # Print context and menu item options.
    fmt("Text: <<" + mention + ">>")
    fmt("Review: " + (review[start_idx:mention_idx] + "<<" + mention + ">>" + \
        review[mention_idx + len(mention):end_idx + 1]).strip())
    print ""
    fmt("0: Not a food item")
    for i, (_, match) in enumerate(matches):
      fmt(str(i+1) + ": " + match.encode(sys.stdout.encoding, errors='replace'), "   ")
    fmt("%d: Can't tell / ambiguous / not listed above" % (len(matches)+1))
    print ""
    match_number = match_prompt(mention, len(matches))

    # If a match was found, get the sentiment and write the output.
    if match_number > 0:
      sentiment = sentiment_prompt(mention)
      menu_item_idx = matches[match_number-1][0]
      # [restaurant id] [review index] [mention index] [mention text] [menu item index] [sentiment]
      output.write("%s\t%d\t%d\t%s\t%s\t%s\n" % \
          (restaurant_id, review_idx, mention_idx, mention, menu_item_idx, sentiment))
    # Otherwise just write it out directly.
    else:
      # match_number is -1 for not a food item, -2 for can't tell.
      output.write("%s\t%d\t%d\t%s\t%d\t-1\n" % \
          (restaurant_id, review_idx, mention_idx, mention, match_number))

    output.flush()

    count += 1
    processed_mentions.append(mention.lower())
    if count == BATCH_SIZE:
      break

  save_file("", "")


def read_files():
  global restaurants

  mentions_file = open(MENTION_FILE, 'r')
  restaurants_file = open(RESTAURANT_FILE, 'r')
  reviews_file = open(REVIEW_FILE, 'r')

  print "Reading file 1/3..."
  i = 0
  restaurant = None
  review_idx = 0
  for line in mentions_file:
    if i == 0:
      i += 1
      continue

    # Restaurant review ID and name line.
    if i % 2 == 1:
      [restaurant_id, review_idx, restaurant_name] = line.strip().split("\t")
      if restaurant_id not in restaurants:
        restaurants[restaurant_id] = Restaurant(restaurant_id, restaurant_name)
      restaurant = restaurants[restaurant_id]

    # Line with mentions.
    else:
      mentions = line.strip().split("\t")
      if not (len(mentions) == 1 and len(mentions[0]) == 0):
        restaurant.mentions[review_idx] = mentions
        for m in mentions:
          all_mentions.append((restaurant.id, int(review_idx), m))

    i += 1

  print "Reading file 2/3..."
  for line in reviews_file:
    all_reviews = json.loads(line)
    id = all_reviews["id"]
    reviews = all_reviews["reviews"]
    reviews = [r["comment"] for r in reviews]
    restaurants[id].reviews = reviews

  print "Reading file 3/3..."
  for line in restaurants_file:
    restaurant_details = json.loads(line)
    id = restaurant_details["id"]
    items = restaurant_details["items"]
    items = [(i["name"], i["description"]) for i in items]
    restaurants[id].menu_items = items


def save_file(_, __, with_error=False, error=None):
  if output is not None:
    if IS_WINDOWS: os.system("cls")
    else:          os.system("clear")
    if with_error:
      print e
      print "\nAn error occurred!\n"
    print "\nSaving file results-%s.txt..." % batch_start
    output.close()
    print "DONE!"
    print "**Please email results-%s.txt to agong@stanford.edu!**" % batch_start
  sys.exit(0)


def main():
  global batch_start
  signal.signal(signal.SIGINT, save_file)
  batch_start = 123984

  print ""
  fmt("Thanks for helping us annotate! You will be given 100 sentences from" +
      " Yelp reviews, each with a phrase highlighted by << >>. You are given" +
      " several options which are menu items. Please choose the menu item " +
      "that best matches the mentioned phrase in the context of the " +
      "sentence.")
  print "\nLoading..."
  time.sleep(15)

  read_files()
  annotate()

try:
  main()
except Exception as e:
  save_file("", "", with_error=True, error=e)

a = open('results-mike.txt', 'r')
b = open('results-angela.txt', 'r')

# One said is food item but other said not.
diff_if_food_item = 0
# Both chose food item but different food item.
diff_food_item = 0
# Different sentiment.
diff_sentiment = 0
# Exact same.
exact_same = 0

count = 0
for aline in a:
  bline = b.readline()
  [afood, asentiment] = aline.split("\t")[4:]
  [bfood, bsentiment] = bline.split("\t")[4:]
  afood = int(afood)
  bfood = int(bfood)

  if afood == -1 and bfood != -1 or afood != -1 and bfood == -1:
    diff_if_food_item += 1

  elif afood > 0 and bfood > 0:
    if asentiment != bsentiment:
      diff_sentiment += 1
    elif afood == bfood:
      exact_same += 1

    if afood != bfood:
      diff_food_item += 1

  elif afood < 0 and bfood < 0:
    exact_same += 1

  else:
    print aline
    print bline

  count += 1

print "Different IF food item: " + str(diff_if_food_item)
print "Different menu item choice: " + str(diff_food_item)
print "Different sentiment: " + str(diff_sentiment)
print "Exact same: " + str(exact_same)
print "Count: " + str(count)

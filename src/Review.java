import org.json.*;

import java.text.*;
import java.util.*;

public class Review {
  /** The index in the Restaurant's list of reviews. */
  public int index;
  public Date date;
  public double rating;
  public String comment;

  public Review(int index, String date, double rating, String comment) {
    this.index = index;
    try {
      SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
      this.date = df.parse(date);
    } catch (ParseException e) {
      this.date = null;
    }
    this.rating = rating;
    this.comment = comment;
  }

  @Override
  public boolean equals(Object o) {
    if (!(o instanceof Review))
      return false;

    Review other = (Review) o;
    return this.index == other.index;
  }

  @Override
  public String toString() {
    return "<REVIEW [" + index + "] " + "date: " + date + ", rating: " +
           rating + ", comment: \"" +
           comment.substring(0, Math.min(20, comment.length())) + "...\">";
  }

  /**
   * Returns a list of Reviews from a JSON array of reviews.
   */
  public static List<Review> parseReviews(JSONArray array) {
    List<Review> reviews = new ArrayList<Review>();
    for (int i = 0; i < array.length(); i++) {
      JSONObject jsonRev = array.getJSONObject(i);
      Review review = new Review(i,
                                 jsonRev.getString("date"),
                                 jsonRev.getDouble("rating"),
                                 jsonRev.getString("comment"));
      reviews.add(review);
    }
    return reviews;
  }
}

import org.json.*;

import java.util.*;

public class Restaurant {
  /** Restaurant ID. */
  public String id;
  public String name;

  public List<Review> reviews;
  public String city;
  public List<String> categories;
  public List<MenuItem> menuItems;

  public String priceRange;
  public double rating;

  public Restaurant(String id, String name, String city, JSONArray categories,
                    JSONArray menuItems, String priceRange, double rating) {
    this.id = id;
    this.name = name;
    this.city = city;
    this.categories = parseCategories(categories);
    this.menuItems = parseMenuItems(menuItems);
    this.priceRange = priceRange;
    this.rating = rating;
  }

  private List<String> parseCategories(JSONArray categories) {
    List<String> cats = new ArrayList<String>();
    for (int i = 0; i < categories.length(); i++) {
      cats.add(categories.getString(i));
    }
    return cats;
  }

  private List<MenuItem> parseMenuItems(JSONArray menuItems) {
    List<MenuItem> items = new ArrayList<MenuItem>();
    for (int i = 0; i < menuItems.length(); i++) {
      items.add(MenuItem.parseMenuItem(items.size(), menuItems.getJSONObject(i)));
    }
    return items;
  }

  @Override
  public boolean equals(Object o) {
    if (!(o instanceof Restaurant))
      return false;

    Restaurant other = (Restaurant) o;
    return this.id.equals(other.id);
  }

  @Override
  public String toString() {
    StringBuilder build = new StringBuilder();
    build.append("<RESTAURANT [" + id + "] " + name + " in " + city + "\n");
    if (reviews != null && menuItems != null) {
      build.append("   REVIEWS:\n");
      for (Review r : reviews) {
        build.append("      " + r.toString() + "\n");
      }
      build.append("   MENU ITEMS:\n");
      for (MenuItem m : menuItems) {
        build.append("      " + m.toString() + "\n");
      }
    }
    build.append(">\n");
    return build.toString();
  }

  public static Restaurant parseRestaurant(JSONObject json) {
    String priceRange = "";
    if (json.getJSONObject("info").has("RestaurantsPriceRange2"))
      priceRange = json.getJSONObject("info").getString("RestaurantsPriceRange2");

    return new Restaurant(json.getString("id"),
                          json.getString("name"),
                          json.getString("city"),
                          json.getJSONArray("categories"),
                          json.getJSONArray("items"),
                          priceRange,
                          json.getDouble("avg_rating"));
  }

  /**
   * Enumeration of possible cities.
   */
  public enum City {
    BOSTON ("boston"),
    CHICAGO ("chicago"),
    LOS_ANGELES ("la"),
    NEW_YORK ("nyc"),
    PHILADELPHIA ("philadelphia"),
    SAN_FRANCISCO ("sf"),
    WASHINGTON_DC ("washington");

    private final String name;
    City(String name) {
      this.name = name;
    }

    // Compare with city.equals(City.LOS_ANGELES)
    public boolean equals(String name) {
      return this.name.equals(name);
    }
  }
}

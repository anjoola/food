import java.util.*;

import util.*;

public class Match {
  /** Fuzzy-matching limit. */
  private static int DAMERAU_LEVENSHTEIN_MAX = 3;

  /** Maximum number of choices before we consider this mention not a food
      item. */
  private static int MAX_NUM_CHOICES = 330;

  /**
   * Matches this mention to a menu item by setting its "guessMenuItem" field.
   *
   * @param m The mention to match with the matcher.
   * @return Possible menu item matches.
   */
  public List<MenuItem> doMatch(Mention m) {
    return new ArrayList<MenuItem>();
  }

  /**
   * Run the matcher on the list of mentions.
   *
   * @param mentions The list of mentions to run on.
   */
  public void run(List<Mention> mentions) {
    for (Mention m : mentions) {
      doMatch(m);
    }
  }

  /**
   * Finds the menu item that exactly matches the mention. The mention and the
   * menu item's name must exactly match.
   */
  public static class ExactMatch extends Match {

    @Override
    public List<MenuItem> doMatch(Mention m) {
      List<MenuItem> possibleMatches = new ArrayList<MenuItem>();
      for (MenuItem item : m.restaurant.menuItems) {
        if (exactlyMatches(m, item)) {
          m.guessMenuItem = item;
          return possibleMatches;
        }
      }
      return possibleMatches;
    }
  }

  /**
   * Finds the menu item that matches the mention if the mention can be found
   * exactly within the menu item's name.
   */
  public static class SubstringMatch extends Match {

    @Override
    public List<MenuItem> doMatch(Mention m) {
      List<MenuItem> possibleMatches = new ArrayList<MenuItem>();
      for (MenuItem item : m.restaurant.menuItems) {
        if (substringMatches(m, item)) {
          possibleMatches.add(item);
        }
      }
      m.guessMenuItem = Match.pickMenuItem(m, possibleMatches);
      return possibleMatches;
    }
  }

  /**
   * Finds the menu item that partially matches the mention (has at least half
   * of the words in common).
   */
  public static class PartialMatch extends Match {

    @Override
    public List<MenuItem> doMatch(Mention m) {
      // Go through all menu items and find one that partially matches this
      // mention. We do this if at least half the words in the mention can be
      // found in the menu item.
      List<MenuItem> possibleMatches = new ArrayList<MenuItem>();
      for (MenuItem item : m.restaurant.menuItems) {
        if (Match.numWordExactMatches(m, item) >
            m.mentionText.split(" ").length / 2) {
          possibleMatches.add(item);
        }
      }
      m.guessMenuItem = Match.pickMenuItem(m, possibleMatches);
      return possibleMatches;
    }
  }

  /**
   * Finds a menu item that fuzzy-matches the mention if all words in the
   * mention fuzzy-match words in the menu item name and description.
   */
  public static class FuzzyMatch extends Match {

    @Override
    public List<MenuItem> doMatch(Mention m) {
      List<MenuItem> possibleMatches = new ArrayList<MenuItem>();
      for (MenuItem item : m.restaurant.menuItems) {
        if (Match.fuzzyMatches(m, item)) {
          possibleMatches.add(item);
        }
      }
      m.guessMenuItem = Match.pickMenuItem(m, possibleMatches);
      return possibleMatches;
    }
  }

  /**
   * Given the remaining choices of menu items narrowed down by the matcher,
   * we still need to decide which menu item to match, or if a match should be
   * made at all.
   *
   * @param m The mention to match.
   * @param items The list of menu items that it could be matched with.
   * @return The menu item chosen, or null if nothing is chosen.
   */
  public static MenuItem pickMenuItem(Mention m, List<MenuItem> items) {
    // If there are too many choices, then probably not a food item.
    if (items.size() > MAX_NUM_CHOICES)
      return null;

    // Pick ones with more word matches.
    List<MenuItem> possibleMatches = new ArrayList<MenuItem>();
    int maxNumWordMatches = 0;
    for (MenuItem item : items) {
      // Found one with more matches. Discard the previous ones.
      int numWordMatches = Match.numWordExactMatches(m, item);
      if (numWordMatches > maxNumWordMatches) {
        possibleMatches.clear();
        possibleMatches.add(item);
        maxNumWordMatches = numWordMatches;
      }
    }

    // Pick the one with the median price.
    Collections.sort(possibleMatches);
    int medianIdx = possibleMatches.size();
    // No matches.
    if (medianIdx == 0)
      return null;
    return possibleMatches.get(medianIdx / 2);
  }

  /**
   * Get the number of words in a mention that are also found in the menu item.
   *
   * @param m The mention.
   * @param item The menu item.
   */
  private static int numWordExactMatches(Mention m, MenuItem item) {
    String[] words = m.mentionText.split(" ");
    int numMatches = 0;
    for (int i = 0; i < words.length; i++) {
      // Give the item name twice the weight.
      if (clean(item.name).contains(clean(words[i])))
        numMatches += 2;
      else if (clean(item.description).contains(clean(words[i])))
        numMatches++;
    }
    return numMatches;
  }

  /**
   * Returns a cleaned-up form of a string by removing special characters and
   * lowercasing the string.
   *
   * @param str The string to clean up.
   * @return The cleaned-up form of the string.
   */
  private static String clean(String str) {
    return str.replaceAll("[^\\w\\s]", "").toLowerCase().trim();
  }

  /**
   * Removes whitespaces from a string.
   */
  private static String trim(String str) {
    return str.replaceAll(" ", "");
  }

  /********************************** UTILS ***********************************/

  /**
   * Returns whether or not this mention exactly matches a menu item's name.
   *
   * @param mention The mention.
   * @param menuItem The menu item.
   */
  public static boolean exactlyMatches(Mention mention, MenuItem menuItem) {
    return clean(menuItem.name).equals(clean(mention.mentionText));
  }

  /**
   * Returns whether or not this mention is a subtring of the menu item's name.
   *
   * @param mention The mention.
   * @param menuItem The menu item.
   */
  public static boolean substringMatches(Mention mention, MenuItem menuItem) {
    return clean(menuItem.name).contains(clean(mention.mentionText));
  };

  /**
   * Returns whether a mention fuzzily-matches a menu item when all words in
   * the mention match a word in the menu item or description, up to
   * DAMERAU_LEVENSHTEIN_MAX distance away.
   *
   * @param m The mention.
   * @param item The menu item.
   * @return Whether or not this mention fuzzy-matches the menu item.
   */
  public static boolean fuzzyMatches(Mention m, MenuItem item) {
    String[] mentionWords = clean(m.mentionText).split(" ");
    int numMatches = numWordsMatch(m, item);
    return numMatches >= mentionWords.length / 2;
  }

  /**
   * Returns the number of words that match between the mention and the menu
   * item. Uses a fuzzy-match.
   *
   * @param m The mention.
   * @param item The menu item.
   * @return The number of words that fuzzy-match.
   */
  public static int numWordsMatch(Mention m, MenuItem item) {
    String[] mentionWords = clean(m.mentionText).split(" ");
    String[] itemWords = clean(item.name + " " + item.description).split(" ");

    DamerauLevenshtein d;
    int numMatches = 0;
    for (int i = 0; i < mentionWords.length; i++) {
      for (int j = 0; j < itemWords.length; j++) {
        if (trim(itemWords[j]).length() < 1) continue;

        try {
          d = new DamerauLevenshtein(mentionWords[i], itemWords[j]);
          if (d.getDHSimilarity() <= DAMERAU_LEVENSHTEIN_MAX) {
            numMatches++;
            break;
          }
        } catch (Exception e) { }
      }
    }
    return numMatches;
  }
}

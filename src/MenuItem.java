import org.json.*;

import java.text.*;
import java.util.*;

public class MenuItem implements Comparable<MenuItem> {
  /** The index of the menu item in the Restaurant's list. */
  public int index;
  public String name;
  public double price;
  public String description;

  public MenuItem(int index, String name, double price, String description) {
    this.index = index;
    this.name = name;
    this.price = price;
    this.description = description;
  }

  @Override
  public int compareTo(MenuItem o) {
    if (this.price < o.price)
      return -1;
    else if (this.price == o.price)
      return 0;
    else
      return 1;
  }

  @Override
  public boolean equals(Object o) {
    if (!(o instanceof MenuItem))
      return false;

    MenuItem other = (MenuItem) o;
    return this.index == other.index;
  }

  @Override
  public String toString() {
    String itemDescription =
        description.length() <= 20 ? description :
        description.substring(0, Math.min(20, description.length()));

    return "<ITEM [" + index + "] " + name + " (" + price + "): \"" +
           itemDescription + "...\">";
  }

  public static MenuItem parseMenuItem(int index, JSONObject json) {
    return new MenuItem(index,
                        json.getString("name"),
                        json.getDouble("price"),
                        json.getString("description"));
  }
}

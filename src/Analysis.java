import org.json.*;

import java.io.*;
import java.util.*;

import util.*;

public class Analysis {
  public static boolean VERBOSE = false;

  /** Mapping of restaurant id -> restaurant information. */
  public static Map<String, Restaurant> restaurants;

  /** Mapping of the training mentions. */
  public static List<Mention> trainMentions;

  /** Mapping of the gold annotated results. */
  public static List<Mention> goldMentions;

  /** All possible amtchers. Dictionary mapping shortname -> Match class. */
  public static Map<String, Class> matchMap;

  public static void main(String[] args) throws IOException {
    Match matcher = getMatcher(System.getenv("matcher"));
    trainMentions = new ArrayList<Mention>();
    goldMentions = new ArrayList<Mention>();

    // Read in files.
    readRestaurantFile();
    readReviewFile();

    // Pre-analysis only needs to run once.
    //preAnalyses();

    System.out.println("Running matcher");
    //readMentionsFile(false, goldMentions);

    readTrainMentionsFile(false, goldMentions);

    // Print out a list of all the words.
    //allWords();

    // Get the right matcher to use.
    if (matcher instanceof SVMMatch) {
      readMentionsFile(true, trainMentions);
      ((SVMMatch) matcher).run(trainMentions, goldMentions);
    } else if (matcher instanceof SVMMatchPredict) {
      readMentionsFile(true, trainMentions);
      ((SVMMatchPredict) matcher).run(trainMentions, goldMentions);
    }
    else {
      matcher.run(goldMentions);
    }

    // Evaluate our results.
    evaluateScore();

    // Do all analyses.
    //doAnalyses();
  }

  /**
   * Get the matcher associated with this name.
   * @param matcherString The string name for the class.
   * @return The matcher requested, or the default matcher if the requested one
   *         is not supplied or found.
   */
  private static Match getMatcher(String matcherString) {
    Match matcher = null;
    Class<?> clazz = null;

    try {
      if (matcherString.equals("SVMMatch")) {
        clazz = Class.forName("SVMMatch");
        matcher = (Match) clazz.newInstance();
      } else if (matcherString.equals("SVMMatchPredict")) {
        clazz = Class.forName("SVMMatchPredict");
        matcher = (Match) clazz.newInstance();
      } else {
        clazz = Class.forName("Match$" + matcherString);
        matcher = (Match) clazz.newInstance();
      }
    } catch (Exception e) {
      try {
        clazz = Class.forName("Match$ExactMatch");
        matcher = (Match) clazz.newInstance();
      } catch (Exception x) { }
    }

    System.out.println("Using " + matcher.getClass().getSimpleName());
    return matcher;
  }

  /**
   * Evaluate our matcher. Prints out the precision, recall, and F1 score.
   */
  private static void evaluateScore() {
    System.out.println("Evaluating results");

    // Number of mentions indicated as food mentions and matched by our matcher,
    // and the number that are correct.
    double selectedMentions = 0.0;
    double correctlySelectedMentions = 0.0;

    // Number of mentions that are indicated as food mentions by annotation in
    // the gold set.
    double totalRelevantMentions = 0.0;
    double truePositives = 0.0;

    // Loop through all the gold mentions and compare our matches with the
    // annotated ones.
    for (Mention m : goldMentions) {
      // Mention matched to a food item.
      if (m.guessMenuItem != null) selectedMentions++;

      // Mention matched correctly to a food item.
      if (m.guessMenuItem != null && m.menuItem != null &&
          m.menuItem.equals(m.guessMenuItem))
        correctlySelectedMentions++;

      // Did gold match this item?r
      if (m.menuItem != null) totalRelevantMentions++;

      // Was our matching correct?
      if (m.menuItem != null && m.menuItem.equals(m.guessMenuItem))
        truePositives++;

      if (VERBOSE) {
        if (m.guessMenuItem == null && m.menuItem != null) {
          System.out.println("\nMention: " + m.mentionText);
          System.out.println("Guess: null");
          System.out.println("Correct: " + m.menuItem.name);
        }
        else if (m.guessMenuItem != null && m.menuItem == null) {
          System.out.println("\nMention: " + m.mentionText);
          System.out.println("Guess: " + m.guessMenuItem.name);
          System.out.println("Correct: null");
        }
        else if (m.menuItem != null && m.guessMenuItem != null &&
                 !m.guessMenuItem.equals(m.menuItem)) {
          System.out.println("\nMention: " + m.mentionText);
          System.out.println("Guess: " + m.guessMenuItem.name);
          System.out.println("Correct: " + m.menuItem.name);
        }
      }
    }

    // Compute precision. Of all the mentions matched by our matcher, how many
    // are actually correct food mentions.
    double precision = correctlySelectedMentions / selectedMentions;
    System.out.println("\n+----------------------------");
    System.out.println("| Precision: " + precision);

    // Compute recall. Of all the correct mentions matched to menu items,
    // how many were found by our matcher?
    double recall = truePositives / totalRelevantMentions;
    System.out.println("| Recall: " + recall);

    // Compute F1 score. This is just:
    //      precision * recall
    // 2 * --------------------
    //      precision + recall
    double f1 = 2 * (precision * recall) / (precision + recall);
    System.out.println("| F1: " + f1);
    System.out.println("+----------------------------\n\n");
  }

  private static void readRestaurantFile() {
    restaurants = new HashMap<String, Restaurant>();

    // File file = new File("..\\data\\restaurants.json"); CHANGED
    File file = new File("..\\data\\restaurants_train.json");
    System.out.println("Reading " + file);
    Scanner sc = null;
    try {
      sc = new Scanner(file);
      String line = "";

      // Read in each line (representing a restaurant).
      while (sc.hasNext()) {
        line = sc.nextLine();
        JSONObject json = new JSONObject(line);
        Restaurant restaurant = Restaurant.parseRestaurant(json);
        restaurants.put(restaurant.id, restaurant);
      }
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  private static void readReviewFile() {
    //File file = new File("..\\data\\reviews.json"); CHANGED
    File file = new File("..\\data\\reviews_train.json");
    System.out.println("Reading " + file);
    Scanner sc = null;
    try {
      sc = new Scanner(file);
      String line = "";

      // Read in each line (a list of reviews for each restaurant).
      while (sc.hasNext()) {
        line = sc.nextLine();
        JSONObject json = new JSONObject(line);
        List<Review> list = Review.parseReviews(json.getJSONArray("reviews"));
        String id = json.getString("id");

        // Match reviews list with the restaurant.
        Restaurant restaurant = restaurants.get(id);
        restaurant.reviews = list;
      }
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  private static void readMentionsFile(boolean isTraining, List<Mention> list) {
    // Mentions file in the form:
    // restaurantId reviewIndex mentionIndex menuItemIndex menuItemText sentiment
    File folder = new File("..\\data\\mentions");
    File[] files = folder.listFiles();
    System.out.println("Reading mentions files");
    int numFiles = isTraining ? (int)(files.length * 0.8) : files.length;

    for (int i = 0; i < numFiles; i++) {
      File file = files[i];
      if (!(file.isFile() && file.toString().contains("results-") &&
            file.toString().endsWith(".txt"))) {
        continue;
      }

      Scanner sc = null;
      try {
        sc = new Scanner(file);
        String line = "";

        while (sc.hasNext()) {
          line = sc.nextLine();
          String[] fields = line.split("\t");

          Restaurant restaurant = restaurants.get(fields[0]);
          Review review = restaurant.reviews.get(Integer.parseInt(fields[1]));
          int mentionIndex = Integer.parseInt(fields[2]);
          String mentionText = fields[3];
          int menuItemIdx = Integer.parseInt(fields[4]);
          MenuItem menuItem =
              menuItemIdx > 0 ? restaurant.menuItems.get(menuItemIdx) : null;
          String sentiment = fields[5];

          list.add(new Mention(restaurant, review, mentionIndex, mentionText,
                               menuItem, sentiment));
        }
      } catch (Exception e) {
        e.printStackTrace();
      }
    }
  }

  private static void readTrainMentionsFile(boolean isTraining, List<Mention> list) {
    // Mentions file in the form:
    // restaurantId reviewIndex mentionIndex menuItemIndex menuItemText sentiment
    File folder = new File("..\\data\\mentions");
    File[] files = folder.listFiles();
    System.out.println("Reading mentions files");
    int numFiles = isTraining ? (int)(files.length * 0.8) : files.length;

    for (int i = 0; i < numFiles; i++) {
      File file = files[i];
      //CHANGED
      if (!(file.isFile() && file.toString().contains("results-TRAIN") &&
            file.toString().endsWith(".txt"))) {
        continue;
      }

      Scanner sc = null;
      try {
        sc = new Scanner(file);
        String line = "";

        while (sc.hasNext()) {
          line = sc.nextLine();
          String[] fields = line.split("\t");

         // if (!restaurants.contains(fields[0])) continue;
          Restaurant restaurant = restaurants.get(fields[0]);
          //System.out.println("um " + restaurant);
          //if (restaurant == null || !restaurant.reviews.contains(Integer.parseInt(fields[1]))) continue;
          //System.out.println("field " + fields[0] + " restaurant " + restaurant);

          Review review = restaurant.reviews.get(Integer.parseInt(fields[1]));
          int mentionIndex = Integer.parseInt(fields[2]);
          String mentionText = fields[3];
          // int menuItemIdx = Integer.parseInt(fields[4]);
          MenuItem menuItem = new MenuItem(-1, "", -1.0, "");
          String sentiment = "";

          list.add(new Mention(restaurant, review, mentionIndex, mentionText,
                               menuItem, sentiment));
        }
      } catch (Exception e) {
        e.printStackTrace();
      }
    }
  }

  private static void allWords() throws IOException {
    File file = new File("..\\data\\analysis\\words.txt");
    PrintWriter pw = new PrintWriter(new FileWriter(file));
    Set<String> words = new HashSet<String>();

    StringBuilder contextAndItems = new StringBuilder();
    int num = 0;
    for (Mention m : goldMentions) {
      // Add mention context.
      contextAndItems.append(m.context);

      // Add possible menu items.
      Match.SubstringMatch matcher = new Match.SubstringMatch();
      for (MenuItem menuItem : matcher.doMatch(m)) {
        contextAndItems.append(" " + menuItem.name + " " + menuItem.description);
      }

      String[] mentionWords = contextAndItems.toString()
                                             .replaceAll("[^a-zA-Z ]", " ")
                                             .toLowerCase().split("\\s+");
      for (int i = 0; i < mentionWords.length; i++) {
        if (!words.contains(mentionWords[i])) {
          pw.println(mentionWords[i]);
          words.add(mentionWords[i]);
        }
      }
      System.out.println("Finished " + num++);
    }
    pw.close();
  }

  private static void preAnalyses() throws IOException {
    File file = null, file2 = null;
    PrintWriter pw = null, pw2 = null;

    // Number of reviews vs. restaurant rating. Output file containing
    // [restaurant rating] [num reviews].
    file = new File("..\\data\\analysis\\num-reviews-ratings.csv");
    pw = new PrintWriter(new FileWriter(file));
    Map<Double, Integer> ratingToReviewCount = new HashMap<Double, Integer>();
    for (Restaurant r : restaurants.values()) {
      if (!ratingToReviewCount.containsKey(r.rating)) {
        ratingToReviewCount.put(r.rating, 0);
      }
      int prevCount = ratingToReviewCount.get(r.rating);
      ratingToReviewCount.put(r.rating, prevCount + r.reviews.size());
    }
    for (Double rating : ratingToReviewCount.keySet()) {
      pw.println(rating + "," + ratingToReviewCount.get(rating));
    }
    pw.close();

    // Review length / word length to restaurant price range. Output file
    // containing [restaurant price] [average review/word length].
    file = new File("..\\data\\analysis\\review-length-price.csv");
    pw = new PrintWriter(new FileWriter(file));
    file2 = new File("..\\data\\analysis\\word-length-price.csv");
    pw2 = new PrintWriter(new FileWriter(file2));
    Map<String, List<Integer>> priceToReview = new HashMap<String, List<Integer>>();
    Map<String, List<Integer>> priceToWordLength = new HashMap<String, List<Integer>>();
    for (Restaurant r : restaurants.values()) {
      if (!priceToReview.containsKey(r.priceRange)) {
        priceToReview.put(r.priceRange, new ArrayList<Integer>());
      }
      priceToReview.get(r.priceRange).add(r.reviews.size());

      if (!priceToWordLength.containsKey(r.priceRange)) {
        priceToWordLength.put(r.priceRange, new ArrayList<Integer>());
      }
      for (Review review : r.reviews) {
        String[] words = review.comment.split(" ");
        for (int i = 0; i < words.length; i++) {
          priceToWordLength.get(r.priceRange).add(words[i].length());
        }
      }
    }
    for (String price : priceToReview.keySet()) {
      double sum = 0.0;
      for (Integer i : priceToReview.get(price)) {
        sum += i.intValue();
      }
      pw.println(price + "," + sum / priceToReview.get(price).size());
    }
    pw.close();
    for (String price : priceToWordLength.keySet()) {
      double sum = 0.0;
      for (Integer i : priceToWordLength.get(price)) {
        sum += i.intValue();
      }
      pw2.println(price + "," + sum / priceToWordLength.get(price).size());
    }
    pw2.close();

    // Location to average restaurant rating. Output file containing
    // [location ] [average rating].
    file = new File("..\\data\\analysis\\location-ratings.csv");
    pw = new PrintWriter(new FileWriter(file));
    Map<String, List<Double>> locationToRatings = new HashMap<String, List<Double>>();
    for (Restaurant r : restaurants.values()) {
      if (!locationToRatings.containsKey(r.city)) {
        locationToRatings.put(r.city, new ArrayList<Double>());
      }
      locationToRatings.get(r.city).add(r.rating);
    }
    for (String city : locationToRatings.keySet()) {
      double sum = 0.0;
      for (Double d : locationToRatings.get(city)) {
        sum += d.doubleValue();
      }
      pw.println(city + "," + sum / locationToRatings.get(city).size());
    }
    pw.close();
  }

  private static void doAnalyses() throws IOException {
    File file = null;
    PrintWriter pw = null;

    // Print out average sentiment/rating for all menu items.
    double avgSentiment = 0.0;
    int count = 0;

    // For each price bucket, find average rating for menu item. Output is of
    // form [item price] [rating].
    file = new File("..\\data\\analysis\\price-vs-rating.csv");
    pw = new PrintWriter(new FileWriter(file));
    for (Mention m : goldMentions) {
      if (m.hasSentiment()) {
        avgSentiment += m.sentiment.value();
        count++;

        pw.println(m.menuItem.price + "," + m.sentiment.value());
      }
    }
    System.out.println("Average sentiment: " + avgSentiment / count);
    pw.close();

    // Grouped by location, average rating for menu items. Output is of form
    // [location] [average rating]
    file = new File("..\\data\\analysis\\location-vs-rating.csv");
    pw = new PrintWriter(new FileWriter(file));
    Map<String, List<Double>> locationToRating = new HashMap<String, List<Double>>();
    for (Mention m : goldMentions) {
      if (!m.hasSentiment()) continue;
      if (!locationToRating.containsKey(m.restaurant.city)) {
        locationToRating.put(m.restaurant.city, new ArrayList<Double>());
      }
      locationToRating.get(m.restaurant.city).add(m.sentiment.value());
    }
    for (String city : locationToRating.keySet()) {
      double total = 0.0;
      for (Double rating : locationToRating.get(city)) {
        total += rating.doubleValue();
      }
      pw.println(city + "," + total / locationToRating.get(city).size());
    }
    pw.close();

    // Grouped by location, average price for menu items. Output is of form
    // [location] [average price]
    file = new File("..\\data\\analysis\\location-vs-price.csv");
    pw = new PrintWriter(new FileWriter(file));
    Map<String, List<Double>> locationToPrice = new HashMap<String, List<Double>>();
    for (Mention m : goldMentions) {
      if (!m.hasSentiment()) continue;
      if (!locationToPrice.containsKey(m.restaurant.city)) {
        locationToPrice.put(m.restaurant.city, new ArrayList<Double>());
      }
      locationToPrice.get(m.restaurant.city).add(m.menuItem.price);
    }
    for (String city : locationToPrice.keySet()) {
      double total = 0.0;
      for (Double price : locationToPrice.get(city)) {
        total += price.doubleValue();
      }
      pw.println(city + "," + total / locationToPrice.get(city).size());
    }
    pw.close();
  }
}

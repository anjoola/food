public class Mention {
  /** The restaurant whose review this mention is found in. */
  public Restaurant restaurant;
  /** The review this mention is found in. */
  public Review review;
  /** Context (the sentence this mention is found in). */
  public String context;
  /** Index of this mention. */
  public int index;
  /** The text for this mention. */
  public String mentionText;
  /** The menu item this mention is matched to. */
  public MenuItem menuItem;
  /** The guess for the menu item. */
  public MenuItem guessMenuItem;
  /** Sentiment. */
  public Sentiment sentiment;
  /** Whether or not we are guessing if this is a food mention. **/
  public boolean isFoodMentionGuess;

  public Mention(Restaurant restaurant, Review review, int index,
                 String mentionText, MenuItem menuItem, String sentiment) {
    this.restaurant = restaurant;
    this.review = review;
    this.index = index;
    this.mentionText = mentionText;
    this.menuItem = menuItem;
    this.guessMenuItem = null;
    this.sentiment = getSentiment(sentiment);
    this.isFoodMentionGuess = false;
    this.context = getReviewContext();
  }

  public boolean isFoodMention() {
    return menuItem != null;
  }

  public boolean hasSentiment() {
    return menuItem != null;
  }

  public Sentiment getSentiment(String sentiment) {
    if (sentiment.equals("pos"))
      return Sentiment.POSITIVE;
    else if (sentiment.equals("neg"))
      return Sentiment.NEGATIVE;
    else
      return Sentiment.NEUTRAL;
  }

  /**
   * Get the context sentence for this mention.
   *
   * @return A string containing the context sentence.
   */
  public String getReviewContext() {
    String reviewText = review.comment;
    int mentionEnd = index + mentionText.length();

    int startIdx = reviewText.lastIndexOf(".", index);
    startIdx = Math.max(reviewText.lastIndexOf("?", index), startIdx);
    startIdx = Math.max(reviewText.lastIndexOf("!", index), startIdx);

    int endIdx = reviewText.indexOf(".", mentionEnd);
    int tempEndIdx = reviewText.indexOf("?", mentionEnd);
    if (tempEndIdx != -1 && tempEndIdx < endIdx) endIdx = tempEndIdx;
    tempEndIdx = reviewText.indexOf("!", mentionEnd);
    if (tempEndIdx != -1 && tempEndIdx < endIdx) endIdx = tempEndIdx;

    startIdx = startIdx != -1 ? startIdx + 1 : 0;
    endIdx = endIdx == -1 ? reviewText.length() : endIdx + 1;
    return reviewText.substring(startIdx, endIdx);
  }

  @Override
  public boolean equals(Object o) {
    if (!(o instanceof Mention))
      return false;

    Mention other = (Mention) o;
    return this.restaurant.equals(other.restaurant) &&
           this.review.equals(other.review) &&
           this.index == other.index;
  }

  /**
   * Enumeration of possible sentiments.
   */
  public enum Sentiment {
    POSITIVE (1.0),
    NEUTRAL (0.0),
    NEGATIVE (-1.0);

    private final double sentiment;
    Sentiment(double sentiment) {
      this.sentiment = sentiment;
    }

    public double value() {
      return sentiment;
    }
  }
}

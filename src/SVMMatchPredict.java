import java.io.*;
import java.util.*;

import util.*;


public class SVMMatchPredict extends Match {
  private static String ISFOOD_TEST_FILE = "svm_files/isfood_test_TRAIN.dat";
  private static String ISFOOD_MODEL_FILE = "svm_files/isfood_model.dat";
  private static String ISFOOD_PREDICT_FILE = "svm_files/isfood_predict_TRAIN.dat";

  private static String MENUITEM_TEST_FILE = "svm_files/menuitem_test_TRAIN.dat";
  private static String MENUITEM_MODEL_FILE = "svm_files/menuitem_model.dat";
  private static String MENUITEM_PREDICT_FILE = "svm_files/menuitem_predict_TRAIN.dat";

  /** Bag-of-words mapping with the key as the word and the value as the index
      of the word if it were put into a list. */
  private static Map<String, Integer> allWords;

  /**
   * Run the matcher. Do training, then do the predictions.
   *
   * @param trainMentions The training data.
   * @param testMentions The test data.
   */
  public void run(List<Mention> trainMentions, List<Mention> testMentions) {
    try {
      // Put words into map.
      wordsToMap("svm_files/words.txt");

      // First figure out if the mention is a food item.
      System.out.println("Figuring out if is a food item");
      predictIsFoodItem(testMentions);

      // Now match to a menu item.
      System.out.println("Matching foods to menu items");
      predictMenuItemMatch(testMentions);
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  private void createIsFoodFile(List<Mention> mentions, String fname)
      throws IOException {
    // Mapping of <feature index> -> 1.
    Map<Integer, Integer> features = null;

    // Create the train file.
    BufferedWriter bw = getBufferedWriter(fname);

    // Output one line for each mention in svm_light format:
    // <1 or -1 if it is a food mention> feature:feature_value [...]
    for (Mention m : mentions) {
      String isFoodMention = m.isFoodMention() ? "1" : "-1";
      bw.write(isFoodMention); // 1 or -1

      // Write out features. The value is always 1 (feature is enabled).
      features = new TreeMap<Integer, Integer>();
      getTrainFoodItemFeatures(m, features);
      for (Integer key: features.keySet()) {
        bw.write(" " + key + ":" + features.get(key));
      }
      bw.write("\n");
    }
    bw.close();
  }

  /**
   * Predict the list of mentions on whether or not it is a food item.
   *
   * @param mentions List of mentions to predict.
   */
  public void predictIsFoodItem(List<Mention> mentions) throws IOException {
    createIsFoodFile(mentions, ISFOOD_TEST_FILE);
    System.out.println("Created test file");

    // Run svm_light via terminal to do predictions.
    execute("lib\\svm_classify " + ISFOOD_TEST_FILE + " " + ISFOOD_MODEL_FILE +
            " " + ISFOOD_PREDICT_FILE);
    System.out.println("Done with predictions");

    // Read in predictions and make guesses.
    readFoodItemPredictionsFile(ISFOOD_PREDICT_FILE, mentions);
  }

  private void createMenuItemFile(List<Mention> mentions, String fname,
                                  boolean isTestFile) throws IOException {
    // Mapping of <feature index> -> <feature value>.
    Map<Integer, Integer> features = null;

    // Create the train file.
    BufferedWriter bw = getBufferedWriter(fname);

    Match.PartialMatch matcher = new Match.PartialMatch();
    for (Mention m : mentions) {
      // Only do this for ones that are food mentions.
      if (isTestFile && !m.isFoodMentionGuess || !isTestFile && !m.isFoodMention())
        continue;

      // One line for each possible menu item match in svm_light format:
      // <1 or -1 if it is a menu item match> feature:feature_value [...]
      boolean wroteMatch = false;
      for (MenuItem menuItem : matcher.doMatch(m)) {
        String isMenuItemMatch =
            (m.menuItem != null && (m.menuItem).equals(menuItem)) ? "1" : "-1";
        if (isMenuItemMatch.equals("1")) wroteMatch = true;
        bw.write(isMenuItemMatch); // 1 or -1

        // Write out features. The value is either 1 (feature is enabled) or
        // a numerical value.
        features = new TreeMap<Integer, Integer>();
        getTrainFoodItemFeatures(m, features);
        getTrainMenuItemFeatures(m, menuItem, features);
        for (Integer key : features.keySet()) {
          bw.write(" " + key + ":" + features.get(key));
        }
        bw.write("\n");
      }

      // Write the real match if it wasn't matched yet.
      if (!wroteMatch && m.menuItem != null) {
        features = new HashMap<Integer, Integer>();
        getTrainFoodItemFeatures(m, features);
        getTrainMenuItemFeatures(m, m.menuItem, features);

        bw.write("1");
        for (Integer key : features.keySet()) {
          bw.write(" " + key + ":" + features.get(key));
        }
        bw.write("\n");
      }
    }
    bw.close();
  }

  /**
   * Predict the list of mentions on whether or not it is a menuItem.
   *
   * @param mentions List of mentions predict.
   */
  public void predictMenuItemMatch(List<Mention> mentions) throws IOException {
    createMenuItemFile(mentions, MENUITEM_TEST_FILE, true);
    System.out.println("Created test file");

    // Run svm_light via terminal to do predictions.
    execute("lib\\svm_classify " + MENUITEM_TEST_FILE + " " +
            MENUITEM_MODEL_FILE + " " + MENUITEM_PREDICT_FILE);
    System.out.println("Done with predictions");

    // Read in predictions and make guesses.
    readMenuItemPredictionsFile(MENUITEM_PREDICT_FILE, mentions);
  }

  /**
   * Gets features for if the word is in, before, or after mention.
   *
   * @param m Mention to write feature for.
   * @param features Empty mapping of features to add to.
   */
  private void getTrainFoodItemFeatures(Mention m, Map<Integer, Integer> features) {
    String[] parts = m.context.split(m.mentionText);
    String during = m.mentionText.toLowerCase();
    String before = "";
    String after = "";
    String[] mentionWords = during.split("\\s+");
    String[] beforeMentionWords;
    String[] afterMentionWords;
    int featureIndex = 0;

    if ((m.context).startsWith(m.mentionText)) {
      after = parts[0].toLowerCase();
      beforeMentionWords = new String[0];
      afterMentionWords = after.split("\\s+");
    }
    else if ((m.context).endsWith(m.mentionText)) {
      before = parts[0].toLowerCase();
      beforeMentionWords = before.split("\\s+");
      afterMentionWords = new String[0];
    }
    else {
      before = parts[0].toLowerCase();
      after = parts[1].toLowerCase();
      beforeMentionWords = before.split("\\s+");
      afterMentionWords = after.split("\\s+");
    }

    // Word is in the mention. Indices 0-[numWords].
    for (int i = 0; i < mentionWords.length; i++) {
      if (allWords.containsKey(mentionWords[i])) {
        featureIndex = allWords.get(mentionWords[i]);
        features.put(featureIndex, 1);
      }
    }
    // Word is before the mention. Indices [numWords]-[2 * numWords].
    for (int i = 0; i < beforeMentionWords.length; i++) {
      if (allWords.containsKey(beforeMentionWords[i])) {
        featureIndex = allWords.get(beforeMentionWords[i]) + allWords.size();
        features.put(featureIndex, 1);
      }
    }
    // Word is after the mention. Indices [2 * numWords]-[3 * numWords].
    for (int i = 0; i < afterMentionWords.length; i++) {
      if (allWords.containsKey(afterMentionWords[i])) {
        featureIndex = allWords.get(afterMentionWords[i]) + allWords.size() * 2;
        features.put(featureIndex, 1);
      }
    }
  }

  /**
   * Gets features that compare the mention with the menu item:
   *   1. How many words overlap between mention and menu item
   *   2. If the mention and menu item is an exact match
   *   3. If they are a substring match
   *   4. If they are a fuzzy match
   *
   * @param m Mention to write feature for.
   * @param menuItem Menu Item to compare with mention.
   * @param features Map of the feature index and value.
   */
  private void getTrainMenuItemFeatures(Mention m, MenuItem menuItem,
                                       Map<Integer, Integer> features) {
    int IDX_START = allWords.size() * 3;

    // 1. How many words overlap between mention and menu item.
    features.put(IDX_START + 1, Match.numWordsMatch(m, menuItem));

    // 2. Is menu item exact match with mention.
    if (Match.exactlyMatches(m, menuItem)) {
      features.put(IDX_START + 2, 1);
    }
    // 3. Is menu item a substring of mention.
    if (Match.substringMatches(m, menuItem)) {
      features.put(IDX_START + 3, 1);
    }
    // 4. Is menu item fuzzy match.
    if (Match.fuzzyMatches(m, menuItem)) {
      features.put(IDX_START + 4, 1);
    }
  }

  /**
   * Read the predictions file and predict if a mention is a food item or not
   *
   * @param fname File name of the predictions file to read from.
   * @param mentions List of mentions to make prediction as a food item or not.
   */
  private static void readFoodItemPredictionsFile(
      String fname, List<Mention> mentions) throws IOException {
    File file = new File(fname);
    Scanner sc = new Scanner(file);
    String line = "";

    // Read in each line (representing predictions) for each mention.
    for (Mention m : mentions) {
      if (!sc.hasNext()) break;

      line = sc.nextLine();
      if (Double.parseDouble(line) > 0) {
        m.isFoodMentionGuess = true;
      }
    }
  }

  /**
   * Read the predictions file and predict which menu item matches with a
   * food item mention
   *
   * @param fname File name of the predictions file to read from.
   * @param mentions List of mentions to make prediction on.
   */
  private static void readMenuItemPredictionsFile(
      String fname, List<Mention> mentions) throws IOException {
    File file = new File(fname);
    Scanner sc = new Scanner(file);
    String line = "";

    // Read in each line (representing predictions) for each mention.
    double max = -2.0;
    MenuItem bestGuessMenuItem = null;

    Match.PartialMatch matcher = new Match.PartialMatch();
    for (Mention m : mentions) {
      // Only make predictions if predicted this is a food mention.
      if (!m.isFoodMentionGuess) continue;

      // Find the best possible menu item match for each mention.
      max = -2.0;
      bestGuessMenuItem = null;
      for (MenuItem menuItem : matcher.doMatch(m)) {
        if (!sc.hasNext()) break;

        line = sc.nextLine();
        if (Double.parseDouble(line) > max) {
          max = Double.parseDouble(line);
          bestGuessMenuItem = menuItem;
        }
      }
      m.guessMenuItem = bestGuessMenuItem;
    }
  }

  /**
   * Gobble up stream for runtime execution to allow execution of arbitrary
   * programs without clogging up the stream.
   */
  public static class StreamGobbler extends Thread {
    InputStream is;

    public StreamGobbler(InputStream is) {
      this.is = is;
    }

    public void run() {
      try {
        InputStreamReader isr = new InputStreamReader(is);
        BufferedReader br = new BufferedReader(isr);
        String line = null;
        while ((line = br.readLine()) != null)
          System.out.println(line);
      } catch (IOException e) {
        e.printStackTrace();
      }
    }
  }

  /**
   * Execute a command in the terminal.
   *
   * @param cmd The command to run.
   * @return The exit value.
   */
  private int execute(String command) {
    try {
      Runtime rt = Runtime.getRuntime();
      System.out.println("Execing " + command);
      Process proc = rt.exec(command);

      // Separate thread to print out error and output.
      StreamGobbler errorGobbler = new StreamGobbler(proc.getErrorStream());
      StreamGobbler outputGobbler = new StreamGobbler(proc.getInputStream());
      errorGobbler.start();
      outputGobbler.start();

      // Wait for thread to finish and print out exit value.
      int exitVal = proc.waitFor();
      return exitVal;
    } catch (Throwable t) {
      t.printStackTrace();
    }
    return -1;
  }

  /**
   * Get a buffered writer.
   *
   * @param fname The file name to get a writer for.
   */
  private BufferedWriter getBufferedWriter(String fname) throws IOException {
    File file = new File(fname);
    if (!file.exists()) {
      file.createNewFile();
    }
    FileWriter fw = new FileWriter(file.getAbsoluteFile(), false);
    BufferedWriter bw = new BufferedWriter(fw);
    return bw;
  }

  /**
   * Reads from a word file and fills the allWords mapping.
   *
   * @param fname File to read from.
   */
  private static void wordsToMap(String fname) throws IOException {
    allWords = new HashMap<String, Integer>();

    // Loop through words in file and put into the map.
    BufferedReader br = new BufferedReader(new FileReader(fname));
    String word = "";
    int index = 0;

    while ((word = br.readLine()) != null) {
      if (!allWords.containsKey(word)) {
        allWords.put(word, index);
        index++;
      }
    }
  }
}
